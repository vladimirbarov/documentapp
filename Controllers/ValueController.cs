﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DocumentApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValueController : ControllerBase
    {
        public async Task<ActionResult<IList<string>>> Get()
        {
            return Ok(await Task.FromResult(new List<string>() { "value1", "value2", "value3", "value4", "value5", }));
        }
    }
}